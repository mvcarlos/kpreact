import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Kpalindrome from './components/kaplindrome'
import Khistorial from './components/historial'

class App extends Component {
	
	render() {
		return (
			<Router >
			<div className="App"> 

				<nav className="navbar navbar-expand-lg navbar-light bg-light">
					<Link className="navbar-brand" to="/">K-palindrome</Link>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav">
						<li className="nav-item active">
						<Link className="nav-link" to="/">Home</Link>
						</li>
						<li className="nav-item">
						<Link className="nav-link" to="/gui">GUI</Link>
						</li>
						<li className="nav-item">
						<Link className="nav-link" to="/history">History</Link>
						</li>
					</ul>
					</div>
				</nav>
				<Switch>
					<Route exact path="/">
					<Home />
					</Route>
					<Route path="/gui">
					<Kpalindrome />
					</Route>
					<Route path="/history">
					<Khistorial />
					</Route>
				</Switch>
			</div>
			</Router>
		);
	}
}

function Home() {
  return (
    <div>
      <h2>Test Kpalindrome </h2>
    </div>
  );
}

export default (App);
