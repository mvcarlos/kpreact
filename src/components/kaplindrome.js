import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { simpleAction } from '../actions/simpleAction';

const mapStateToProps = state => ({
	...state
})

const mapDispatchToProps = dispatch => ({
	simpleAction: (state) => dispatch(simpleAction(state))
})

class Kpalindrome extends React.Component {

    
    constructor(props) {
        super(props);
        this.state = {
            s: '',
            k:'',
            showResults:false,
            result:'',
            cached:'',
            loading:false,
            disabled: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }


    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        //console.log(value)
        this.setState({
          [name]: value
        });
    }

    componentWillUnmount() {
        clearTimeout(this.turnOffMessage);
    }
  
    handleSubmit = async (event) => {
        event.preventDefault();
        
        this.setState({ loading: true, disabled: true });
        const params = {
            "word": this.state.s,
            "k": this.state.k
        };
        await axios.post('http://challenge.dev.lab.plugbox.io:8080/api/kpalindrome/', params, {
		 headers: {
			  'content-type': 'application/json',
		 },
        }).then(res => {
            
            this.setState({ loading: false , disabled: false });
            this.setState({ showResults: true,  result: res.data.answer, cached: res.data.cached });
            this.props.simpleAction(this.state);
            this.turnOffMessage = setTimeout( () => {
                this.setState({ showResults: false });
            }, 5000);
            /*console.log(res);
            console.log(res.data);*/
        }).catch(e =>{
            this.setState({ loading: false, disabled: false });
            console.log(e);
        }) 

    }
  
    render() {
      return (
        <div className="d-flex justify-content-center">
            <div className="w-30 p-3">
            { this.state.showResults && this.state.result ?  <div id="results" className="alert alert-success" role="alert">
                The word is K-palindrome
            </div>: null }

            { this.state.showResults && !this.state.result ?  <div id="results" className="alert alert-danger" role="alert">
                The word is not K-palindrome
            </div>: null }
                <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="kaplindrome">Word</label>
                    <input type="text" name="s" className="form-control" id="kaplindrome" minLength="3" maxLength="100" pattern="[a-z]+" value={this.state.s} onChange={this.handleInputChange} 
                    disabled = {(this.state.disabled)? "disabled" : ""} required/>
                </div>
                <div className="form-group">
                    <label htmlFor="k">K</label>
                    <input type="number" name="k" className="form-control" id="k" min="1" max="20" value={this.state.k} onChange={this.handleInputChange} 
                    disabled = {(this.state.disabled)? "disabled" : ""} required/>
                </div>
                <button  disabled = {(this.state.disabled)? "disabled" : ""} type="submit" className="btn btn-primary">Calculate{this.state.loading ?<div className="spinner-border spinner-border-sm text-light ml-2" role="status"
                >
                </div>:null}</button>
                
                </form>
            </div>
           
        </div>
      );
    }
  }


  export default connect(mapStateToProps, mapDispatchToProps)(Kpalindrome);