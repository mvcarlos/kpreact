import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
	...state
})

class Khistory extends React.Component {

    
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    componentDidMount = async ()  => {
        let data = this.props;
        //console.log(JSON.stringify(data))
        this.setState({ data: data.simpleReducer });
    }

    render() {
      return (
        <div className="d-flex justify-content-center">
            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">Word</th>
                    <th scope="col">K</th>
                    <th scope="col">Cached</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.data.map((obj, i) => (
                    <tr key={i}>
                    <td>{obj.result.s}</td>
                    <td>{obj.result.k}</td>
                    <td>{obj.result.cached.toString()}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
      );
    }
  }


  export default connect(mapStateToProps, null)(Khistory);
