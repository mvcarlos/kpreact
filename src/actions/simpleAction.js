export const simpleAction = (state) => dispatch => {
    dispatch({
        type: 'SIMPLE_ACTION',
        payload: state
    });
}
